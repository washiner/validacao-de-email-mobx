import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:untitled/app/stores/email_store.dart';

class HomeScreeanMobx extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final emailStore = EmailStore();
    void _continuar(){
      print("ativado");
    }
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Validação de Email Mobx"),
      ),
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                onChanged: emailStore.trocarEmail,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    labelText: "Email",
                    labelStyle: TextStyle(fontSize: 20)
                ),
              ),
              Observer(
                builder: (_){
                  return MaterialButton(
                  minWidth: double.infinity,
                  child: Text("Continuar", style: TextStyle(fontSize: 20),),
                  onPressed: emailStore.botaoAtivado ? _continuar : null,
                  textColor: Colors.white,
                  color: Colors.blue,
                  disabledColor: Colors.grey,
                );
                  },
              )
            ],
          ),
        ),
      ),
    );
  }
}
