// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'email_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$EmailStore on _EmailStore, Store {
  final _$botaoAtivadoAtom = Atom(name: '_EmailStore.botaoAtivado');

  @override
  bool get botaoAtivado {
    _$botaoAtivadoAtom.reportRead();
    return super.botaoAtivado;
  }

  @override
  set botaoAtivado(bool value) {
    _$botaoAtivadoAtom.reportWrite(value, super.botaoAtivado, () {
      super.botaoAtivado = value;
    });
  }

  final _$_EmailStoreActionController = ActionController(name: '_EmailStore');

  @override
  void trocarEmail(dynamic value) {
    final _$actionInfo = _$_EmailStoreActionController.startAction(
        name: '_EmailStore.trocarEmail');
    try {
      return super.trocarEmail(value);
    } finally {
      _$_EmailStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
botaoAtivado: ${botaoAtivado}
    ''';
  }
}
